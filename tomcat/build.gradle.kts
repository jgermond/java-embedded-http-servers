plugins {
    kotlin("jvm")
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.apache.tomcat.embed:tomcat-embed-core:9.0.30")
    implementation("org.apache.tomcat.embed:tomcat-embed-logging-juli:9.0.0.M6")
    implementation("com.google.code.gson:gson:2.8.6")
    implementation(project(":domain"))
}

tasks {
    jar {
        manifest {
            attributes["Main-Class"] = "com.jgermond.MainKt"
        }

        from(configurations.runtimeClasspath.get().filter { it.name.endsWith("jar") }.map { zipTree(it) })
    }
}
