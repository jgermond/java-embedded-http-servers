package com.jgermond.tomcat

import org.apache.catalina.Context
import org.apache.catalina.connector.Connector
import org.apache.catalina.filters.CorsFilter
import org.apache.catalina.filters.RemoteAddrFilter
import org.apache.catalina.startup.Tomcat
import org.apache.tomcat.util.descriptor.web.FilterDef
import org.apache.tomcat.util.descriptor.web.FilterMap
import org.apache.tomcat.util.net.SSLHostConfig
import java.io.File
import javax.servlet.Filter
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletResponse

fun main() {
    val tomcat = Tomcat()
    tomcat.setHostname("localhost")

    val context: Context = tomcat.addContext("/", File(".").absolutePath)

    addFilter(context, RemoteAddrFilter::class.java, mapOf("allow" to "127.0.0.1"))
    addFilter(context, CorsFilter::class.java, mapOf(CorsFilter.PARAM_CORS_ALLOWED_ORIGINS to "*"))
    addFilter(context, CustomHeaderFilter::class.java)

    Tomcat.addServlet(context, examinationsCollectionServletName, examinationsCollectionServlet)
    context.addServletMappingDecoded("/examination", examinationsCollectionServletName)
    Tomcat.addServlet(context, examinationsObjectServletName, examinationsObjectServlet)
    context.addServletMappingDecoded("/examination/*", examinationsObjectServletName)

    tomcat.connector = createSslConnector()
    tomcat.start()
    tomcat.server.await()
}

private fun createSslConnector(): Connector {
    val sslHostConfig = SSLHostConfig()
    sslHostConfig.certificateKeystoreFile = "../localhost.p12"
    sslHostConfig.certificateKeystorePassword = "myPassword"

    val connector = Connector()
    connector.port = 8080
    connector.addSslHostConfig(sslHostConfig)
    connector.setProperty("SSLEnabled", "true")
    return connector
}

private fun <T> addFilter(context: Context, filterClass: Class<T>, parameters: Map<String, String> = emptyMap()) {
    val filterDef = FilterDef()
    filterDef.filterClass = filterClass.name
    filterDef.filterName = filterClass.simpleName
    parameters.forEach { (name: String, value: String) ->
        filterDef.addInitParameter(name, value)
    }
    context.addFilterDef(filterDef)

    val filterMap = FilterMap()
    filterMap.filterName = filterClass.simpleName
    filterMap.addURLPattern("/*")
    context.addFilterMap(filterMap)
}

fun matchPath(pattern: String, path: String): Map<String, String>? {
    val patternSegments = pattern.split("/")
    val pathSegments = path.split("/")

    if (patternSegments.size != pathSegments.size) return null

    return patternSegments
        .zip(pathSegments)
        .filter { it.first != it.second }
        .map { (key, value) ->
            if (!key.startsWith("{") || !key.endsWith("}")) {
                return null
            } else {
                Pair(key.removePrefix("{").removeSuffix("}"), value)
            }
        }.toMap()
}
