package com.jgermond.tomcat

import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.jgermond.domain.ExaminationContent
import com.jgermond.domain.ExaminationPersistence
import javax.servlet.http.HttpServlet
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse


private val gson = Gson()

private fun Gson.fromJsonOrNull(json: String): ExaminationContent? =
    try {
        fromJson(json, com.jgermond.domain.ExaminationContent::class.java)
    } catch (e: JsonSyntaxException) {
        null
    }

const val examinationsCollectionServletName = "examinationsCollectionServlet"
val examinationsCollectionServlet: HttpServlet = object : HttpServlet() {
    override fun doGet(req: HttpServletRequest, resp: HttpServletResponse) {
        val w = resp.writer
        w.write(gson.toJson(ExaminationPersistence.getAllExaminations()))
        w.flush()
        w.close()
    }

    override fun doPost(req: HttpServletRequest, resp: HttpServletResponse) {
        gson.fromJsonOrNull(req.reader.readText()).let {
            when (it) {
                null -> resp.status = 400
                else -> ExaminationPersistence.addExamination(it)
            }
        }
    }
}

const val examinationsObjectServletName = "examinationsObjectServlet"
val examinationsObjectServlet: HttpServlet = object : HttpServlet() {
    private fun extractId(req: HttpServletRequest): String? =
        matchPath("/examination/{id}", req.requestURI)?.get("id")

    override fun doGet(req: HttpServletRequest, resp: HttpServletResponse) {
        val id = extractId(req)
        if (id == null) {
            resp.status = 404
            return
        }
        val examination = ExaminationPersistence.getExamination(id)

        if (examination.isPresent) {
            val w = resp.writer
            w.write(gson.toJson(ExaminationPersistence.getExamination(id).get()))
            w.flush()
            w.close()
        } else {
            resp.status = 404
        }
    }

    override fun doPut(req: HttpServletRequest, resp: HttpServletResponse) {
        val id = extractId(req)
        if (id == null) {
            resp.status = 404
            return
        }

        gson.fromJsonOrNull(req.reader.readText()).let {
            when (it) {
                null -> resp.status = 400
                else -> ExaminationPersistence.updateExamination(id, it)
            }
        }
    }

    override fun doDelete(req: HttpServletRequest, resp: HttpServletResponse) {
        val id = extractId(req)
        if (id == null) {
            resp.status = 404
            return
        }
        ExaminationPersistence.deleteExamination(id)
    }
}
