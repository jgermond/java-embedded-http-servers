package com.jgermond.tomcat

import javax.servlet.Filter
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletResponse

class CustomHeaderFilter : Filter {
    override fun doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain) {
        (response as HttpServletResponse).addHeader("My-Header", "Awesome")
        chain.doFilter(request, response)
    }
}
