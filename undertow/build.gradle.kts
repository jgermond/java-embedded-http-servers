plugins {
    kotlin("jvm")
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("io.undertow:undertow-core:2.0.29.Final")
    implementation("com.google.code.gson:gson:2.8.6")
    implementation(project(":domain"))
}

tasks {
    jar {
        manifest {
            attributes["Main-Class"] = "com.jgermond.undertow.MainKt"
        }

        from(configurations.runtimeClasspath.get().filter { it.name.endsWith("jar") }.map { zipTree(it) })
    }
}
