package com.jgermond.undertow

import com.jgermond.undertow.handler.ExaminationHttp
import com.jgermond.undertow.mbean.CustomUndertowStatistics
import io.undertow.Handlers
import io.undertow.Undertow
import io.undertow.UndertowOptions
import io.undertow.server.HttpHandler
import io.undertow.server.RoutingHandler
import io.undertow.server.handlers.IPAddressAccessControlHandler
import io.undertow.server.handlers.SetHeaderHandler
import java.io.FileInputStream
import java.lang.management.ManagementFactory
import java.security.KeyStore
import javax.management.ObjectName
import javax.net.ssl.KeyManager
import javax.net.ssl.KeyManagerFactory

// Domain routing handler
val examinationRoutingHandler: RoutingHandler = RoutingHandler()
    .get("/examination") { ExaminationHttp.executeGetAll(exchange = it) }
    .post("/examination") { ExaminationHttp.executePost(exchange = it) }
    .get("/examination/{id}") { ExaminationHttp.executeGetOne(exchange = it) }
    .put("/examination/{id}") { ExaminationHttp.executePut(exchange = it) }
    .delete("/examination/{id}") { ExaminationHttp.executeDelete(exchange = it) }

// Add custom action for each received request
val customLogHandler: HttpHandler = HttpHandler {
    println("The path `${it.requestPath}` has been called.")
    examinationRoutingHandler.handleRequest(it)
}

// Add a header in all response
val allowCorsHandler: SetHeaderHandler = Handlers.header(customLogHandler, "Access-Control-Allow-Origin", "*")

// Accept only request from IP in the list
val ipAccessControlHandler: IPAddressAccessControlHandler = Handlers.ipAccessControl(allowCorsHandler, false)
    .addAllow("127.0.0.1")
    .addAllow("192.168.0.*")

fun main() {
    // Create and start the Undertow server
    val server = Undertow.builder()
        .addHttpsListener(8080, "localhost", getKeyManagers(), null)
        .setHandler(ipAccessControlHandler)
        .setServerOption(UndertowOptions.ENABLE_STATISTICS, true)
        .build()
    server.start()

    // Create and register the MBean with Undertow statistics
    val mBean = CustomUndertowStatistics(server.listenerInfo)
    val name = ObjectName("com.jgermond.undertow.mbean:type=CustomUndertowStatistics")
    ManagementFactory.getPlatformMBeanServer().registerMBean(mBean, name)
}

// Read key store file created by `httpsCert.sh` script
private fun getKeyManagers(): Array<KeyManager>? {
    val keyStorePassword = "myPassword".toCharArray()
    val ks = KeyStore.getInstance("pkcs12")
    ks.load(FileInputStream("./localhost.p12"), keyStorePassword)
    val kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm())
    kmf.init(ks, keyStorePassword)
    return kmf.keyManagers
}
