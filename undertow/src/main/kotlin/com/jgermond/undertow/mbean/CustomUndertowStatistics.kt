package com.jgermond.undertow.mbean

import io.undertow.Undertow

class CustomUndertowStatistics(private val listeners: List<Undertow.ListenerInfo>) : CustomUndertowStatisticsMBean {
    private val connectorStatistics = listeners.map { it.connectorStatistics }

    override fun getActiveConnections() =
        connectorStatistics.map { it.activeConnections }
            .reduce { a, b -> a + b }

    override fun getActiveRequests() =
        connectorStatistics.map { it.activeRequests }
            .reduce { a, b -> a + b }

    override fun getErrorCount() =
        connectorStatistics.map { it.errorCount }
            .reduce { a, b -> a + b }

    override fun getRequestCount() =
        connectorStatistics.map { it.requestCount }
            .reduce { a, b -> a + b }
}
