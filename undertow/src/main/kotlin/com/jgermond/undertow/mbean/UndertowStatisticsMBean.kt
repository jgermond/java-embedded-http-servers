package com.jgermond.undertow.mbean

interface CustomUndertowStatisticsMBean {
    fun getActiveConnections(): Long
    fun getActiveRequests(): Long
    fun getErrorCount(): Long
    fun getRequestCount(): Long
}