package com.jgermond.undertow.handler

import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.jgermond.domain.ExaminationContent
import com.jgermond.domain.ExaminationPersistence
import io.undertow.server.HttpServerExchange
import io.undertow.util.Headers

object ExaminationHttp {
    private val gson = Gson()

    fun executeGetAll(exchange: HttpServerExchange) {
        exchange.responseHeaders.put(Headers.CONTENT_TYPE, "application/json")
        exchange.responseSender.send(gson.toJson(ExaminationPersistence.getAllExaminations()))
    }

    fun executeGetOne(exchange: HttpServerExchange) {
        exchange.responseHeaders.put(Headers.CONTENT_TYPE, "application/json")
        exchange.queryParameters["id"]?.first.let {
            when (it) {
                null -> exchange.statusCode = 400
                else -> ExaminationPersistence.getExamination(it).let { examination ->
                    if (examination.isPresent) {
                        exchange.responseSender.send(gson.toJson(examination.get()))
                    } else {
                        exchange.statusCode = 404
                    }
                }
            }
        }
    }

    fun executePost(exchange: HttpServerExchange) {
        exchange.requestReceiver.receiveFullString { exchange, message ->
            gson.fromJsonOrNull(message).let {
                when (it) {
                    null -> exchange.statusCode = 400
                    else -> ExaminationPersistence.addExamination(it)
                }
            }
        }
    }

    fun executePut(exchange: HttpServerExchange) {
        exchange.requestReceiver.receiveFullString { exchange, message ->
            exchange.queryParameters["id"]?.first.let { id ->
                when (id) {
                    null -> exchange.statusCode = 400
                    else -> gson.fromJsonOrNull(message).let { examinationContent ->
                        when (examinationContent) {
                            null -> exchange.statusCode = 400
                            else -> ExaminationPersistence.updateExamination(id, examinationContent)
                        }
                    }
                }
            }
        }
    }

    fun executeDelete(exchange: HttpServerExchange) {
        exchange.queryParameters["id"]?.first.let {
            when (it) {
                null -> exchange.statusCode = 400
                else -> ExaminationPersistence.deleteExamination(it)
            }
        }
    }

    private fun Gson.fromJsonOrNull(json: String): ExaminationContent? =
        try {
            fromJson(json, com.jgermond.domain.ExaminationContent::class.java)
        } catch (e: JsonSyntaxException) {
            null
        }
}
