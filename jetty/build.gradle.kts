plugins {
    kotlin("jvm")
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.eclipse.jetty:jetty-servlet:9.4.28.v20200408")
    implementation("org.eclipse.jetty:jetty-servlets:9.4.28.v20200408")
    implementation("org.eclipse.jetty:jetty-jmx:9.4.28.v20200408")
    implementation("com.google.code.gson:gson:2.8.6")
    implementation(project(":domain"))
}

tasks {
    jar {
        manifest {
            attributes["Main-Class"] = "com.jgermond.jetty.MainKt"
        }

        from(configurations.runtimeClasspath.get().filter { it.name.endsWith("jar") }.map { zipTree(it) })
    }
}
