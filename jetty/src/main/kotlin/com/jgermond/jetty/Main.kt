package com.jgermond.jetty

import org.eclipse.jetty.jmx.MBeanContainer
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.server.ServerConnector
import org.eclipse.jetty.server.handler.HandlerCollection
import org.eclipse.jetty.server.handler.InetAccessHandler
import org.eclipse.jetty.servlet.ServletContextHandler
import org.eclipse.jetty.util.ssl.SslContextFactory
import java.lang.management.ManagementFactory

fun main() {
    val server = Server()
    server.addConnector(createHttpsConnector(server))

    val servletContextHandler = ServletContextHandler()
    servletContextHandler.addServlet(ExaminationsCollectionServlet::class.java, "/examination")
    servletContextHandler.addServlet(ExaminationsObjectServlet::class.java, "/examination/*")

    val handlerCollection = HandlerCollection()
    val inetAccessHandler = InetAccessHandler()
    inetAccessHandler.include("127.0.0.1")
    inetAccessHandler.handler = servletContextHandler
    handlerCollection.addHandler(inetAccessHandler)
    server.handler = handlerCollection

    server.addBean(MBeanContainer(ManagementFactory.getPlatformMBeanServer()))

    server.start()
    server.join()
}

private fun createHttpsConnector(server: Server): ServerConnector {
    val sslContextFactory: SslContextFactory = SslContextFactory.Server()
    sslContextFactory.keyStorePath = "./localhost.p12"
    sslContextFactory.setKeyStorePassword("myPassword")

    val httpsConnector = ServerConnector(server, sslContextFactory)
    httpsConnector.host = "localhost"
    httpsConnector.port = 8080

    return httpsConnector
}

fun matchPath(pattern: String, path: String): Map<String, String>? {
    val patternSegments = pattern.split("/")
    val pathSegments = path.split("/")

    if (patternSegments.size != pathSegments.size) return null

    return patternSegments
        .zip(pathSegments)
        .filter { it.first != it.second }
        .map { (key, value) ->
            if (!key.startsWith("{") || !key.endsWith("}")) {
                return null
            } else {
                Pair(key.removePrefix("{").removeSuffix("}"), value)
            }
        }.toMap()
}
