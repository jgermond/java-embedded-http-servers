rootProject.name = "java-embedded-http-servers"

include("domain")
include("undertow")
include("tomcat")
include("jetty")
include("netty")
