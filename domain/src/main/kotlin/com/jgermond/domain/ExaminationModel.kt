package com.jgermond.domain

data class Examination(
    val id: String,
    val content: ExaminationContent
)

data class ExaminationContent(
    val patientName: String,
    val doctorName: String,
    val examination: String
)
