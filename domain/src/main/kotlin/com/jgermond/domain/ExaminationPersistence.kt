package com.jgermond.domain

import java.util.*

object ExaminationPersistence {
    private var nextId = 1
    private fun getNextId() = (nextId++).toString()
    private val examinations = mutableMapOf<String, ExaminationContent>(
        getNextId() to ExaminationContent(patientName = "Alice", doctorName = "Doctor Foo", examination = "Slight cough"),
        getNextId() to ExaminationContent(patientName = "Bob", doctorName = "Doctor Bar", examination = "Arm fracture")
    )

    fun getAllExaminations() = examinations.map { (id, content) ->
        Examination(id, content)
    }

    fun getExamination(id: String): Optional<Examination> =
        examinations[id].let {
            when (it) {
                null -> Optional.empty()
                else -> Optional.of(Examination(id, it))
            }
        }

    fun addExamination(examinationContent: ExaminationContent) {
        examinations[getNextId()] = examinationContent
    }

    fun updateExamination(id: String, examinationContent: ExaminationContent) {
        examinations[id] = examinationContent
    }

    fun deleteExamination(id: String) {
        examinations.remove(id)
    }
}
