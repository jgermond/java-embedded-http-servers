package com.jgermond.netty

import io.netty.handler.codec.http.*

data class Route(
    val method: HttpMethod,
    val pattern: String,
    val action: (Map<String, String>) -> FullHttpResponse
)

class Router {
    private val routes: MutableList<Route> = mutableListOf()
    fun get(pattern: String, action: (Map<String, String>) -> FullHttpResponse) = add(HttpMethod.GET, pattern, action)
    fun post(pattern: String, action: (Map<String, String>) -> FullHttpResponse) = add(HttpMethod.POST, pattern, action)
    fun put(pattern: String, action: (Map<String, String>) -> FullHttpResponse) = add(HttpMethod.PUT, pattern, action)
    fun delete(pattern: String, action: (Map<String, String>) -> FullHttpResponse) = add(HttpMethod.DELETE, pattern, action)

    private fun add(method: HttpMethod, pattern: String, action: (Map<String, String>) -> FullHttpResponse): Router {
        routes.add(Route(method, pattern, action))
        return this
    }

    fun match(request: FullHttpRequest): FullHttpResponse? {
        routes.filter { it.method == request.method() }
            .forEach { route ->
                val params = matchPath(route.pattern, request.uri())
                if (params != null) {
                    return route.action(params)
                }
            }
        return DefaultFullHttpResponse(
            HttpVersion.HTTP_1_1,
            HttpResponseStatus.NOT_FOUND
        )
    }

    private fun matchPath(pattern: String, path: String): Map<String, String>? {
        val patternSegments = pattern.split("/")
        val pathSegments = path.split("/")

        if (patternSegments.size != pathSegments.size) return null

        return patternSegments
            .zip(pathSegments)
            .filter { it.first != it.second }
            .map { (key, value) ->
                if (!key.startsWith("{") || !key.endsWith("}")) {
                    return null
                } else {
                    Pair(key.removePrefix("{").removeSuffix("}"), value)
                }
            }.toMap()
    }
}
