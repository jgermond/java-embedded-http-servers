package com.jgermond.netty

import io.netty.bootstrap.ServerBootstrap
import io.netty.channel.*
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.SocketChannel
import io.netty.channel.socket.nio.NioServerSocketChannel
import io.netty.handler.codec.http.*
import io.netty.handler.codec.http.cors.CorsConfigBuilder
import io.netty.handler.codec.http.cors.CorsHandler
import io.netty.handler.ipfilter.IpFilterRuleType
import io.netty.handler.ipfilter.IpSubnetFilterRule
import io.netty.handler.ipfilter.RuleBasedIpFilter
import io.netty.handler.ssl.SslHandler
import io.netty.util.CharsetUtil
import java.io.FileInputStream
import java.security.KeyStore
import javax.net.ssl.KeyManager
import javax.net.ssl.KeyManagerFactory
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLEngine

class AppHandler : ChannelInboundHandlerAdapter() {
    override fun channelRead(ctx: ChannelHandlerContext, msg: Any) {
        val request = msg as FullHttpRequest

        val router = Router()
            .get("/examination") { ExaminationHttp.executeGetAll() }
            .get("/examination/{id}") { ExaminationHttp.executeGetOne(it.getValue("id")) }
            .post("/examination") { ExaminationHttp.executePost(request.content().toString(CharsetUtil.UTF_8)) }
            .put("/examination/{id}") { ExaminationHttp.executePut(it.getValue("id"), request.content().toString(CharsetUtil.UTF_8)) }
            .delete("/examination/{id}") { ExaminationHttp.executeDelete(it.getValue("id")) }

        ctx.writeAndFlush(router.match(request))
            .addListener(ChannelFutureListener.CLOSE)
    }
}

class CustomHeaderOutboundHandler : ChannelOutboundHandlerAdapter() {
    override fun write(ctx: ChannelHandlerContext, msg: Any, promise: ChannelPromise) {
        val response = msg as DefaultFullHttpResponse
        response.headers().add(HttpHeaderNames.CONTENT_TYPE, "application/json")
        response.headers().setInt(HttpHeaderNames.CONTENT_LENGTH, response.content().readableBytes())
        ctx.write(response, promise)
    }
}

fun main() {
    val bossGroup: EventLoopGroup = NioEventLoopGroup()
    val workerGroup: EventLoopGroup = NioEventLoopGroup()
    try {
        val b = ServerBootstrap()
        b.group(bossGroup, workerGroup)
            .channel(NioServerSocketChannel::class.java)
            .childHandler(
                object : ChannelInitializer<SocketChannel>() {
                    override fun initChannel(ch: SocketChannel) {
                        val maxContentLength = 1048576
                        val ipFilter = RuleBasedIpFilter(
                            IpSubnetFilterRule("127.0.0.1", 32, IpFilterRuleType.ACCEPT),
                            IpSubnetFilterRule("0.0.0.0", 0, IpFilterRuleType.REJECT)
                        )
                        val corsConfig = CorsConfigBuilder.forAnyOrigin().build()

                        val pipeline = ch.pipeline()
                        pipeline.addLast(ipFilter)
                        pipeline.addLast(getSslHandler())
                        pipeline.addLast(HttpRequestDecoder())
                        pipeline.addLast(HttpResponseEncoder())
                        pipeline.addLast(HttpObjectAggregator(maxContentLength))
                        pipeline.addLast(CorsHandler(corsConfig))
                        pipeline.addLast(CustomHeaderOutboundHandler())
                        pipeline.addLast(AppHandler())
                    }
                }
            )

        val f: ChannelFuture = b.bind(8080).sync()
        f.channel().closeFuture().sync()
    } finally {
        workerGroup.shutdownGracefully()
        bossGroup.shutdownGracefully()
    }
}

private fun getKeyManagers(): Array<KeyManager>? {
    val keyStorePassword = "myPassword".toCharArray()
    val ks = KeyStore.getInstance("pkcs12")
    ks.load(FileInputStream("./localhost.p12"), keyStorePassword)
    val kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm())
    kmf.init(ks, keyStorePassword)
    return kmf.keyManagers
}

private fun getSslHandler(): SslHandler {
    val serverSSLContext: SSLContext = SSLContext.getInstance("TLS")
    serverSSLContext.init(getKeyManagers(), null, null)

    val sslEngine: SSLEngine = serverSSLContext.createSSLEngine()
    sslEngine.useClientMode = false
    return SslHandler(sslEngine)
}
