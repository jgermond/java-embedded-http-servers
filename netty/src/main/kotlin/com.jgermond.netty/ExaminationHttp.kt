package com.jgermond.netty

import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.jgermond.domain.ExaminationContent
import com.jgermond.domain.ExaminationPersistence
import io.netty.buffer.Unpooled
import io.netty.handler.codec.http.DefaultFullHttpResponse
import io.netty.handler.codec.http.FullHttpResponse
import io.netty.handler.codec.http.HttpResponseStatus
import io.netty.handler.codec.http.HttpVersion
import io.netty.util.CharsetUtil

object ExaminationHttp {
    private val gson = Gson()

    fun executeGetAll(): FullHttpResponse =
        DefaultFullHttpResponse(
            HttpVersion.HTTP_1_1,
            HttpResponseStatus.OK,
            Unpooled.copiedBuffer(
                gson.toJson(ExaminationPersistence.getAllExaminations()),
                CharsetUtil.UTF_8
            )
        )

    fun executeGetOne(id: String): FullHttpResponse {
        ExaminationPersistence.getExamination(id).let { examination ->
            return if (examination.isPresent) {
                DefaultFullHttpResponse(
                    HttpVersion.HTTP_1_1,
                    HttpResponseStatus.OK,
                    Unpooled.copiedBuffer(
                        gson.toJson(examination.get()),
                        CharsetUtil.UTF_8
                    )
                )
            } else {
                DefaultFullHttpResponse(
                    HttpVersion.HTTP_1_1,
                    HttpResponseStatus.NOT_FOUND
                )
            }
        }
    }

    fun executePost(requestBody: String): FullHttpResponse {
        return gson.fromJsonOrNull(requestBody).let { examinationContent ->
            when (examinationContent) {
                null -> DefaultFullHttpResponse(
                    HttpVersion.HTTP_1_1,
                    HttpResponseStatus.BAD_REQUEST
                )
                else -> {
                    ExaminationPersistence.addExamination(examinationContent)
                    DefaultFullHttpResponse(
                        HttpVersion.HTTP_1_1,
                        HttpResponseStatus.OK
                    )
                }
            }
        }
    }

    fun executePut(id: String, requestBody: String): FullHttpResponse {
        return gson.fromJsonOrNull(requestBody).let { examinationContent ->
            when (examinationContent) {
                null -> DefaultFullHttpResponse(
                    HttpVersion.HTTP_1_1,
                    HttpResponseStatus.BAD_REQUEST
                )
                else -> {
                    ExaminationPersistence.updateExamination(id, examinationContent)
                    DefaultFullHttpResponse(
                        HttpVersion.HTTP_1_1,
                        HttpResponseStatus.OK
                    )
                }
            }
        }
    }

    fun executeDelete(id: String): FullHttpResponse {
        ExaminationPersistence.deleteExamination(id)
        return DefaultFullHttpResponse(
            HttpVersion.HTTP_1_1,
            HttpResponseStatus.OK
        )
    }

    private fun Gson.fromJsonOrNull(json: String): ExaminationContent? =
        try {
            fromJson(json, com.jgermond.domain.ExaminationContent::class.java)
        } catch (e: JsonSyntaxException) {
            null
        }
}
