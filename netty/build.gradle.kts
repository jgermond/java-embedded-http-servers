plugins {
    kotlin("jvm")
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("io.netty:netty-all:4.1.49.Final")
    implementation("com.google.code.gson:gson:2.8.6")
    implementation(project(":domain"))
}

tasks {
    jar {
        manifest {
            attributes["Main-Class"] = "com.jgermond.netty.MainKt"
        }

        from(configurations.runtimeClasspath.get().filter { it.name.endsWith("jar") }.map { zipTree(it) })
    }
}
