plugins {
    kotlin("jvm") version "1.3.61"
}

allprojects {
    repositories {
        mavenLocal()
    }
    group = "com.jgermond"
    repositories {
        jcenter()
    }
}

subprojects {
    apply(plugin = "java")
    version = "1.0"
}
