openssl genrsa -out localhost.key 4096
openssl req -new -key localhost.key -out localhost.csr -subj /C=FR/ST=France/L=Rouen/O=JGermond/CN=localhost
openssl x509 -req -days 365 -in localhost.csr -signkey localhost.key -out localhost.crt
openssl pkcs12 -export -in localhost.crt -inkey localhost.key -out localhost.p12 -passout pass:myPassword